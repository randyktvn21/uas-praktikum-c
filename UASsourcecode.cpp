/*
Nama : Randy Oktaviana Hertland
Kelas : IF-1B
NIM : 301230053
*/

#include <iostream>
using namespace std;

// Fungsi untuk menghitung nilai akhir
double hitungNilai(double absen, double tugas, double quiz, double uts, double uas)
{
    return ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;
}

// Prosedur untuk menentukan huruf mutu berdasarkan nilai
void tentukanHurufMutu(double nilai, char &Huruf_Mutu)
{
    if (nilai > 85 && nilai <= 100)
        Huruf_Mutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        Huruf_Mutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        Huruf_Mutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        Huruf_Mutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        Huruf_Mutu = 'E';
}
