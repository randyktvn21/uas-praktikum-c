/*
Nama : Randy Oktaviana Hertland
Kelas : IF-1B
NIM : 301230053
*/

#include <iostream>
using namespace std;
#include "UASsourcecode.cpp"

int main()
{
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;

    quiz = 50;
    absen = 100;
    uts = 40;
    uas = 60;
    tugas = 80;

    cout << "Absen = " << absen << " UTS = " << uts << endl;
    cout << "Tugas = " << tugas << " UAS = " << uas << endl;
    cout << "Quiz = " << quiz << endl;

    nilai = hitungNilai(absen, tugas, quiz, uts, uas);
    tentukanHurufMutu(nilai, Huruf_Mutu);

    cout << "Huruf Mutu : " << Huruf_Mutu << endl;

    return 0;
}

